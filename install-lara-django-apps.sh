#!/bin/bash

# Load environment variables from .env file
set -a
[ -f .env ] && . .env
set +a

# Get the DOCKER_IMAGE_TAG variable
DOCKER_IMAGE_TAG="${DOCKER_IMAGE_TAG}"

LARA_DJANGO_APPS_FILENAME="lara_django_apps.txt"

# Construct the URL for the requirements file
REQUIREMENTS_URL="https://gitlab.com/LARAsuite/lara-django/-/raw/${DOCKER_IMAGE_TAG}/docker/requirements/${LARA_DJANGO_APPS_FILENAME}"

# Download the requirements file
curl -L "$REQUIREMENTS_URL" -o "$LARA_DJANGO_APPS_FILENAME"

# Append the DOCKER_IMAGE_TAG to the file
echo "# FROM DOCKER IMAGE $DOCKER_IMAGE_TAG" >> $LARA_DJANGO_APPS_FILENAME

# Install the requirements using pip
pip install -r "$LARA_DJANGO_APPS_FILENAME"
