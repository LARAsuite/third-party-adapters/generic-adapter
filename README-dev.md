# Generic Adapter

## Installation

Create a virtual environment and activate it.

```
python3 -m venv venv
source venv/bin/activate
```

Add to an environment file `.env` the following variables:
| Variable name | Description |
|---|---|
| DOCKER_IMAGE_TAG | The Gitlab image tag / commit hash containing the Lara Django project version you want to use |

Run the script `install-lara-django-apps.sh`:

```bash
./install-lara-django-apps.sh
```

It should download the requirements file and install them all into your local virtual environment.
