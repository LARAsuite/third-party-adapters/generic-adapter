
# Acknowledgements and Credits

The x2lara project thanks


Contributors
------------

* Mickey Kim <mickey.kim@genomicsengland.co.uk>  ! Thanks for the phantastic cookiecutter template !


Development Lead
----------------

* mark doerr <mark@uni-greifswald.de>