# x2lara

Generic Database 2 LARA adapter

## Features


## Documentation

The Documentation can be found here: [https://larasuite.gitlab.io/third-party-adapters/generic-adapter](https://larasuite.gitlab.io/third-party-adapters/generic-adapter).

## Development

    # clone repository

    https://gitlab.com/LARAsuite/third-party-adapters/generic-adapter.git

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development

## Credits

This package was created with Cookiecutter* and the `opensource/templates/cookiecutter-pypackage`* project template.

[Cookiecutter](https://github.com/audreyr/cookiecutter )
[opensource/templates/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage)
