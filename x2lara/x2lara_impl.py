"""_____________________________________________________________________

:PROJECT: x2lara

* Main module implementation *

:details:  Main module implementation.

.. note:: -
.. todo:: -
________________________________________________________________________
"""


import logging

import grpc
from .x2lara_interface import GreeterInterface

import lara_django_data_grpc.v1.lara_django_data_pb2 as lara_django_data_pb2
import lara_django_data_grpc.v1.lara_django_data_pb2_grpc as lara_django_data_pb2_grpc

grpc_server_host = "localhost"
grpc_server_port = 50051


class HelloWorld(GreeterInterface):
    def __init__(self) -> None:
        """Implementation of the GreeterInterface"""

    def greet_the_world(self, name: str) -> str:
        """greeting module - adds a name to a greeting

        :param name: person to greet
        :type name: str
        """
        logging.debug(f"Greeting: {name}")
        return f"Hello world, {name} !"

    def add_data(self, name, uuid, data_xml, datetime_created, description) -> lara_django_data_pb2.DataResponse:
        """Adds one data entry"""
        logging.debug("Adding data entry: {name}")

        with grpc.insecure_channel(f"{grpc_server_host}:{grpc_server_port}") as channel:
            data_client = lara_django_data_pb2_grpc.DataControllerStub(channel)

            response = data_client.Create(
                lara_django_data_pb2.DataRequest(
                    name=name,
                    UUID=uuid,
                    datetime_created=str(datetime_created),
                    data_XML=data_xml,
                    description=description,
                )
            )
            print(response, end="|")

            return response

    def get_data(self, data_id) -> lara_django_data_pb2.DataResponse:
        """Gets one data entry"""
        logging.debug("Getting data entry: {data_id}")

        with grpc.insecure_channel(f"{grpc_server_host}:{grpc_server_port}") as channel:
            data_client = lara_django_data_pb2_grpc.DataControllerStub(channel)

            response = data_client.Retrieve(lara_django_data_pb2.DataRetrieveRequest(data_id=data_id))
            print(response, end="|")

            return response

    def remove_data(self, data_id) -> None:
        """Removes one data entry"""
        with grpc.insecure_channel(f"{grpc_server_host}:{grpc_server_port}") as channel:
            data_client = lara_django_data_pb2_grpc.DataControllerStub(channel)
            data_client.Destroy(lara_django_data_pb2.DataRequest(data_id=data_id))

    def update_data(
        self, data_id, name, uuid, data_xml, datetime_created, description
    ) -> lara_django_data_pb2.DataResponse:
        """Updates one data entry"""
        with grpc.insecure_channel(f"{grpc_server_host}:{grpc_server_port}") as channel:
            data_client = lara_django_data_pb2_grpc.DataControllerStub(channel)
            response = data_client.Update(
                lara_django_data_pb2.DataRequest(
                    data_id=data_id,
                    name=name,
                    UUID=uuid,
                    datetime_created=str(datetime_created),
                    data_XML=data_xml,
                    description=description,
                )
            )

            return response
