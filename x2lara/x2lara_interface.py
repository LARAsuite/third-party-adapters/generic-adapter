"""_____________________________________________________________________

:PROJECT: x2lara

* Main module formal interface. *

:details: In larger projects, formal interfaces are helping to define a trustable contract.
          Currently there are two commonly used approaches: 
          [ABCMetadata](https://docs.python.org/3/library/abc.html) or [Python Protocols](https://peps.python.org/pep-0544/)

       see also:
       ABC metaclass
         - https://realpython.com/python-interface/
         - https://dev.to/meseta/factories-abstract-base-classes-and-python-s-new-protocols-structural-subtyping-20bm

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


# here is a
from abc import ABCMeta, abstractmethod
from typing import Dict, Any


class GreeterInterface(metaclass=ABCMeta):
    """Greeter formal Interface
    TODO: test, if ABC baseclass is wor
    """

    @abstractmethod
    def greet_the_world(self, name: str) -> Dict[str, Any]:
        """greeting module - adds a name to a greeting

        :param name: person to greet
        :type name: str
        """

    @abstractmethod
    def get_data(self, data_id) -> Dict[str, Any]:
        """Gets one data entry"""

    @abstractmethod
    def update_data(self, data_id, name, data_xml) -> Dict[str, Any]:
        """Updates one data entry"""

    @abstractmethod
    def add_data(self, name, uuid, data_xml, datetime_created, description="") -> str:
        """Adds one data entry"""

    @abstractmethod
    def remove_data(self, data_id) -> None:
        """Removes one data entry"""
