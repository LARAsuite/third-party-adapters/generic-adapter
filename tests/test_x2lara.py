#!/usr/bin/env python
"""Tests for `x2lara` package."""
# pylint: disable=redefined-outer-name

from typing import List, Generator
from uuid import uuid4
import datetime
import pytest
from grpc import RpcError
from x2lara.x2lara_interface import GreeterInterface
from x2lara.x2lara_impl import HelloWorld


@pytest.fixture
def hello_world():
    yield HelloWorld()


@pytest.mark.skip(reason="no way of currently testing this")
@pytest.fixture
def cleanup_data(hello_world: HelloWorld) -> Generator[List[str], None, None]:
    created_data_ids = []  # Store the IDs of created data

    yield created_data_ids

    # Perform cleanup
    for data_id in created_data_ids:
        try:
            hello_world.remove_data(data_id)
        except RpcError as err:
            print("Error removing", err)


@pytest.mark.skip(reason="no way of currently testing this")
@pytest.fixture
def data_added(hello_world: HelloWorld, cleanup_data: List[str]):
    uuid_str = str(uuid4())
    name = f"test_data-{uuid_str}"
    data_xml = "<xml>data test</xml>"
    datetime_created = datetime.datetime.now().isoformat() + "Z"
    description = "Test description"

    response = hello_world.add_data(
        name,
        uuid_str,
        data_xml,
        datetime_created,
        description,
    )

    yield response

    # Perform cleanup
    cleanup_data.append(response.data_id)


def test_GreeterInterface():
    """testing the formal interface (GreeterInterface)"""
    assert issubclass(HelloWorld, GreeterInterface)


def test_HelloWorld(hello_world: HelloWorld):
    """Testing HelloWorld class"""
    name = "yvain"
    assert hello_world.greet_the_world(name) == f"Hello world, {name} !"


@pytest.mark.skip(reason="no way of currently testing this")
def test_add_data(hello_world: HelloWorld, cleanup_data: List[str]):
    uuid_str = str(uuid4())
    name = f"test_data1-{uuid_str}"
    data_xml = "<xml>data test 1</xml>"
    datetime_created = datetime.datetime.now().isoformat() + "Z"
    description = "Test description"

    response = hello_world.add_data(
        name,
        uuid_str,
        data_xml,
        datetime_created,
        description,
    )

    cleanup_data.append(response.data_id)

    assert response.name == name
    assert response.UUID == uuid_str
    assert response.data_XML == data_xml
    assert response.description == description
    assert response.datetime_created == datetime_created


@pytest.mark.skip(reason="no way of currently testing this")
def test_get_data(hello_world: HelloWorld, cleanup_data: List[str]):
    uuid_str = str(uuid4())
    name = f"test_data2-{uuid_str}"
    data_xml = "<xml>data test 2</xml>"
    datetime_created = datetime.datetime.now().isoformat() + "Z"
    description = "Test description"

    response = hello_world.add_data(
        name,
        uuid_str,
        data_xml,
        datetime_created,
        description,
    )

    cleanup_data.append(response.data_id)

    retrieved_data = hello_world.get_data(response.data_id)

    assert retrieved_data.name == name
    assert retrieved_data.UUID == uuid_str
    assert retrieved_data.data_XML == data_xml
    assert retrieved_data.description == description
    assert retrieved_data.datetime_created == datetime_created


@pytest.mark.skip(reason="no way of currently testing this")
def test_update_data(hello_world: HelloWorld, data_added):
    data_id = data_added.data_id

    uuid_str_updated = str(uuid4())
    name_updated = f"test_data3-{uuid_str_updated}"
    data_xml_updated = "<xml>data test 3 updated</xml>"
    datetime_created_updated = datetime.datetime.now().isoformat() + "Z"
    description_updated = "Test description updated"

    response = hello_world.update_data(
        data_id, name_updated, uuid_str_updated, data_xml_updated, datetime_created_updated, description_updated
    )

    assert response.name == name_updated
    assert response.UUID == uuid_str_updated
    assert response.data_XML == data_xml_updated
    assert response.description == description_updated
    assert response.datetime_created == datetime_created_updated


@pytest.mark.skip(reason="no way of currently testing this")
def test_remove_data(hello_world: HelloWorld, data_added):
    hello_world.remove_data(data_added.data_id)

    with pytest.raises(RpcError) as execution_info:
        hello_world.get_data(data_added.data_id)

    assert "not_found" in str(execution_info.value).lower()
